import numpy as np
import matplotlib.pyplot as plt

# Parameters
n = 6 # (number of levers)
e_r = 0.2 # (exploration rate)
seed = 123 # (fixator of randomnes)
thres = 0.4 # (threshold to put zero chance to win for some levers)
periods = 1500 # (number of periods)
decrease_rate = 1.001 # a denominator to decrease explorationary rate

np.random.seed(seed=seed)

p = np.random.uniform(size=(n))
for i in range(n):
    if np.random.rand() < thres:
        p[i] = 0.

# True probabilities to win
print()
print('-------------------------------------------------------------------')
print('Unknown for agent probabilities to win using corresponding lever')
print('-------------------------------------------------------------------')
print()
p = p / np.sum(p)

print(p)
print()
print()

rewards = np.ones(shape=(n,))
wins = np.ones(shape=(n,))
regrets = []

for i in range(periods):
    probs = np.zeros(shape=(n))
    for j in range(n):
        probs[j] = (1 - e_r) * wins[j] / np.sum(wins) + e_r / n
    k = np.random.choice(n, p=probs)
    x = np.zeros(shape=(n,))
    x[k] = 1 if np.random.rand() < p[k] else 0
    wins *= np.exp(e_r * x / n)
    regrets.append(p.dot(probs) * periods)

# The policy to act according the results of the algorithm
print('-------------------------------------------------------------------')
print('Policy to maximize the rewards after ' + str(periods) + 'number of attempts')
print('-------------------------------------------------------------------')
print()
print(probs)
print()


# Plot regrets

plt.figure()
plt.plot(list(range(periods)), regrets, '-', c='blue')
plt.plot(list(range(periods)), np.ones(shape=(periods,)) * np.max(p) * periods, '--', c='red')
plt.show()

# Decreasing exploration rate

rewards = np.ones(shape=(n,))
wins = np.ones(shape=(n,))
regrets = []

periods *= 3

for i in range(periods):
    probs = np.zeros(shape=(n))
    for j in range(n):
        probs[j] = (1 - e_r) * wins[j] / np.sum(wins) + e_r / n
    k = np.random.choice(n, p=probs)
    x = np.zeros(shape=(n,))
    x[k] = 1 if np.random.rand() < p[k] else 0
    wins *= np.exp(e_r * x / n)
    e_r /= decrease_rate
    regrets.append(p.dot(probs) * periods)

# The policy to act according the results of the algorithm with decreasing exploration rate
print('-------------------------------------------------------------------')
print('Policy to maximize the rewards after ' + str(periods) + ' number of attempts with decreasing exploration rate')
print('-------------------------------------------------------------------')
print()
print(probs)
print()

# Plot regrets with decreasing exploration rate

plt.figure()
plt.plot(list(range(periods)), regrets, '-', c='blue')
plt.plot(list(range(periods)), np.ones(shape=(periods,)) * np.max(p) * periods, '--', c='red')
plt.show()